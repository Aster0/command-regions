<div align="center">

<table align="center"><tr><td align="center" width="9999">
<img src=https://proxy.spigotmc.org/07ca1dc1471fd304e2d9dd40566d24abae9f4ed3?url=https%3A%2F%2Fcdn.discordapp.com%2Fattachments%2F592549946469777419%2F593066472935522304%2FArtboard_19.png align="center" width="650" alt="Project icon">



**Command Regions REBORN** is out, what's the difference between this and the old plugin?
- TOTAL recode of the plugin.
- OPTIMIZATION
- UNLIMITED regions
- 
(Click [here](https://www.spigotmc.org/resources/command-regions-reborn-execute-action-s-when-entering-exiting-regions-1-8-x-1-14-2.68727/) to be redirected to the Spigot resource)


</td></tr></table>


<br>![Image of overView](https://proxy.spigotmc.org/1b9ce90817728948e2423e2684ec91735e314062?url=https%3A%2F%2Fcdn.discordapp.com%2Fattachments%2F592549946469777419%2F592637690327990274%2FArtboard_14_copy_4.png)

Command Regions allow you to create **UNLIMITED regions** and execute actions on enter/leave, for your players.

**NOTE:** This plugin requires no dependencies!




<br>

---
<br> 

![Image of features](https://proxy.spigotmc.org/58f8075edd93be4e7ffc5412571f26de4c7b3b42?url=https%3A%2F%2Fcdn.discordapp.com%2Fattachments%2F592549946469777419%2F592637086113202177%2FArtboard_14_copy.png)</div>
* **UNLIMITED** regions.
* **UNLIMITED** commands on enter/leave
* **CUSTOM** message on enter/leave
* **VERY** customizable - I have taken the effort to make this plugin almost a 100% customizable even though this is an Administrative plugin tool. You're welcome! :)

<br>

Things you can use this plugin for -
A place for RTP as shown in the above video/GIF,
A VIP place,
really, it's based on your imagination!




<div align="center">

---

<br>

![Image of wiki](https://proxy.spigotmc.org/881f638a65f64e58a8c4a847a820c3f2a4ff3d6f?url=https%3A%2F%2Fcdn.discordapp.com%2Fattachments%2F592549946469777419%2F592637082757890058%2FArtboard_14_copy_3.png)
<br>

If you have any doubts on any features of the plugin, visit our [wiki](https://gitlab.com/Aster0/command-regions/wikis/home). It boasts a really comprehensive wiki, allowing you to hopefully understand each and every features of the plugin.

---

<br>

Got an issue or a suggestion? Make a new post [here](https://gitlab.com/Aster0/command-regions/issues), I am regularly checking them and will reply to you in less than a day max. If you ever need support on how to use a certain feature of the plugin, join the Discord support server here.


<br>

